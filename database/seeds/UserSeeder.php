<?php

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
        $superadmin = User::create(
            [
                'name' => 'Super Admin',
                'username' => 'superadmin',
                'password' => bcrypt('superadmin')
            ]
        );

        $superadmin->attachRole(App\Role::where('name', 'superadmin')->first());
    }
}
