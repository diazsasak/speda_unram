<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\Permission;

class EntrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        Permission::truncate();
        DB::table('role_user')->delete();
        DB::table('permission_role')->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // role
        $superadmin = Role::create([
        	'name' => 'superadmin',
        	'display_name' => 'Super Admin',
        	'description' => 'Semua akses'
        ]);
        $admin = Role::create([
        	'name' => 'admin',
        	'display_name' => 'Admin',
        	'description' => 'User Management'
        ]);
        $enterprise = Role::create([
        	'name' => 'enterprise',
        	'display_name' => 'Enterprise',
        	'description' => 'Lihat informasi'
        ]);
        $operator = Role::create([
        	'name' => 'operator',
        	'display_name' => 'Operator',
        	'description' => 'Akses bervariasi'
        ]);

        // permission
        $p1 = Permission::create([
        	'name' => 'manage-user',
        	'display_name' => 'Manajemen User'
        ]);
        $p2 = Permission::create([
        	'name' => 'upload-data-bank',
        	'display_name' => 'Upload data bank'
        ]);
        $p3 = Permission::create([
        	'name' => 'upload-data-spp',
        	'display_name' => 'Upload data SPP'
        ]);
        $p4 = Permission::create([
        	'name' => 'add-kku',
        	'display_name' => 'Tambah Kontrak UPT'
        ]);
        $p5 = Permission::create([
        	'name' => 'edit-kku',
        	'display_name' => 'Edit Kontrak UPT'
        ]);
        $p6 = Permission::create([
        	'name' => 'delete-kku',
        	'display_name' => 'Hapus Kontrak UPT'
        ]);
        $p7 = Permission::create([
        	'name' => 'add-rekan-kerja',
        	'display_name' => 'Tambah Rekan Kerja'
        ]);
        $p8 = Permission::create([
        	'name' => 'edit-rekan-kerja',
        	'display_name' => 'Edit Rekan Kerja'
        ]);
        $p9 = Permission::create([
        	'name' => 'delete-rekan-kerja',
        	'display_name' => 'Hapus Rekan Kerja'
        ]);

        // permission role
        $superadmin->attachPermissions([
        	$p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9
        ]);
        $admin->attachPermissions([
        	$p1
        ]);
    }
}
