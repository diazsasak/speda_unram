<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tagihan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_mahasiswa')->unsigned();
            // $table->integer('id_spp')->unsigned();
            $table->string('tahun');
            $table->date('tanggal_bayar')->nullable();
            //FK
            $table->foreign('id_mahasiswa')
                ->references('id')->on('mahasiswa')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            //FK
            // $table->foreign('id_spp')
            //     ->references('id')->on('spp')
            //     ->onUpdate('cascade')
            //     ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tagihan');
    }
}
