<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Grade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('grade', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // $table->integer('id_prodi')->unsigned();
            // $table->integer('kode_grade');
            $table->integer('nominal');
            $table->string('tahun');
            $table->timestamps();
            //FK
            // $table->foreign('id_prodi')
            //     ->references('id')->on('prodi')
            //     ->onUpdate('cascade')
            //     ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('grade');
    }
}
