<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KontrakKerjasamaUpt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontrak_kerjasama_upt', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('no_kontrak');
            $table->integer('besar_dana');
            $table->integer('id_rekan_kerja')->unsigned();
            $table->string('no_rks');
            $table->string('pic');
            $table->string('dokumen_kontrak');
            $table->string('dokumen_transfer_dana');
            $table->date('tanggal_transfer');
            //FK
            $table->foreign('id_rekan_kerja')
                ->references('id')->on('rekan_kerja')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kontrak_kerjasama_upt');
    }
}
