<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mahasiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nim');
            $table->string('nama');
            $table->integer('id_prodi')->unsigned();
            $table->integer('id_grade')->unsigned();
            //FK
            $table->foreign('id_prodi')
            ->references('id')->on('prodi')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->foreign('id_grade')
            ->references('id')->on('grade')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mahasiswa');
    }
}
