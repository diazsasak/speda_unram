<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Spp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spp', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ta');
            $table->string('grade');
            $table->string('nominal');
            $table->integer('id_prodi')->unsigned();
            $table->integer('id_sk')->unsigned();
            //FK
            $table->foreign('id_prodi')
            ->references('id')->on('prodi')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            //FK
            $table->foreign('id_sk')
            ->references('id')->on('sk')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('spp');
    }
}
