<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upt extends Model
{
	protected $table = 'kontrak_kerjasama_upt';
	protected $fillable = ['no_kontrak','besar_dana','id_rekan_kerja','no_rks','pic','dokumen_kontrak','dokumen_transfer_dana','tanggal_transfer'];
	public function rk()
	{
		return $this->belongsTo('App\RekanKerja', 'id_rekan_kerja');
	}
}
