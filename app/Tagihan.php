<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{
    protected $table = 'tagihan';
    protected $fillable = ['id_mahasiswa', 'tanggal_bayar', 'tahun'];

    public function mahasiswa()
    {
        return $this->belongsTo('App\Mahasiswa','id_mahasiswa','id');
    }
}
