<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $table = 'prodi';
    protected $fillable = ['id_fakultas','nama'];

    public function mahasiswa()
    {
        return $this->hasMany('App\Mahasiswa', 'id_prodi', 'id');
    }
}
