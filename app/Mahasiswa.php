<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $fillable = ['nim', 'nama', 'id_prodi', 'id_grade'];

    public function tagihan()
    {
        return $this->hasMany('App\Tagihan', 'id_mahasiswa', 'id');
    }
    public function prodi()
    {
        return $this->belongsTo('App\Prodi','id_prodi','id');
    }
}
