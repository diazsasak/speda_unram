<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekanKerja extends Model
{
    //
    protected $table = 'rekan_kerja';
    protected $fillable = ['id', 'rekan_kerja'];
    public function upt()
    {
        return $this->hasMany('App\Upt', 'id_rekan_kerja', 'id');
    }
}
