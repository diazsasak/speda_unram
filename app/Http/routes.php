<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Blade::setContentTags('<%', '%>');

Route::get('/', function(){
	if (Auth::check()) {
		return redirect('dashboard');
	} else {
		return view('home');
	}
	
});

//LOGIN
Route::get('login','AuthController@login');
Route::post('login','AuthController@postLogin');

Route::group(['middleware' => 'auth'], function(){
	// user management
	Route::group(['middleware' => ['permission:manage-user']], function(){
		Route::resource('user', 'UserController');
	});
	//Dashboard
	Route::get('/dashboard', 'DashboardController@index');

	//UPT
	Route::get('upt', 'UptController@index');
	Route::group(['middleware' => ['permission:add-kku']], function(){
		Route::get('upt/create', 'UptController@create');
		Route::post('upt', 'UptController@store');
	});
	Route::group(['middleware' => ['permission:edit-kku']], function(){
		Route::get('upt/{id}/edit', 'UptController@edit');
		Route::put('upt/{id}', 'UptController@update');
	});
	Route::group(['middleware' => ['permission:delete-kku']], function(){
		Route::delete('upt/{id}', 'UptController@destroy');
	});

	//REKAN KERJA
	Route::get('rekankerja', 'RKController@index');
	Route::group(['middleware' => ['permission:add-rekan-kerja']], function(){
		Route::get('rekankerja/create', 'RKController@create');
		Route::post('rekankerja', 'RKController@store');
	});
	Route::group(['middleware' => ['permission:edit-rekan-kerja']], function(){
		Route::get('rekankerja/{id}/edit', 'RKController@edit');
		Route::put('rekankerja/{id}', 'RKController@update');
	});
	Route::group(['middleware' => ['permission:delete-rekan-kerja']], function(){
		Route::delete('rekankerja/{id}', 'RKController@destroy');
	});
	
	//GRADE
	Route::resource('grade', 'GradeController');
	//logout
	Route::get('logout', 'AuthController@logout');
	//upload data SPP
	Route::group(['middleware' => ['permission:upload-data-spp']], function(){
		Route::get('upload_spp', 'SppController@uploadSpp');
		Route::post('upload_spp', 'SppController@postUploadSpp');
	});
	//upload data Bank
	Route::group(['middleware' => ['permission:upload-data-bank']], function(){
		Route::get('upload_data_bank', 'SppController@uploadDataBank');
		Route::post('upload_data_bank', 'SppController@postUploadDataBank');
	});
	//status bayar
	Route::get('status_pembayaran_spp', 'SppController@index');
	Route::get('post_status_pembayaran_spp', 'SppController@postIndex');
	Route::get('laporan', 'ReportController@index');
	Route::get('fakultas', 'FakultasController@index');
	Route::post('get_data_fakultas', 'FakultasController@getDataFakultas');
	Route::post('get_data_all_fakultas', 'FakultasController@getDataAllFakultas');
	Route::post('get_data_spp_ukt', 'FakultasController@getDataSppUkt');
});