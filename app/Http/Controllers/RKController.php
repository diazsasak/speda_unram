<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Session;
use App\RekanKerja;

class RKController extends Controller
{
	public function index(){
		$data = RekanKerja::all();
		return view('rekan_kerja.index', compact('data'));
	}

	 public function create(){
	 	return view('rekan_kerja.create');
	 }

	public function store(Request $request){
		$data = $request->all();
		RekanKerja::create($data);
		// sending back with message
		Session::flash('success', 'Data tersimpan'); 
		return redirect('/rekankerja');
  	}

  	public function destroy($id){
  		$data = RekanKerja::find($id);
  		$data->delete();
  		Session::flash('success', 'Data terhapus'); 
		  return redirect('/rekankerja');
  	}

  	public function edit($id){
  		$data = RekanKerja::find($id);
  		return view ('/rekan_kerja.edit_rekan_kerja')->with('data', $data);
  		var_dump($data);
  	}
  	public function update(Request $request, $id){
  		RekanKerja::find($id)->update($request->all());
  		return redirect('rekankerja');
  	}

}
