<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\Grade;

class GradeController extends Controller
{
    //
    public function index(){
    	$data=Grade::all();
    	return view('grade.index')->with('data', $data);
    }

    public function create(){
    	return view('grade.create');
    }
    public function store(Request $request){
    	$data = $request->all();
		Grade::create($data);
		// sending back with message
		Session::flash('success', 'Upload successfully'); 
		return redirect('/grade');
    }
}
