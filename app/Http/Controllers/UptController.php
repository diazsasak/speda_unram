<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use File;
use Session;
use App\Upt;
use App\RekanKerja;
use App\Http\Requests\UptRequest;
use App\Http\Requests\UptEditRequest;

class UptController extends Controller
{
	public function index(){
		$data = Upt::all();
		return view('upt.index', compact('data'));
	}

	public function create(){
		$rekankerja = RekanKerja::all();
		return view('upt.create')->with('rekankerja', $rekankerja);
	}

	public function store(UptRequest $request){
		$extension = $request->file('dokumen_kontrak')->getClientOriginalExtension(); // getting image extension
		$fileName = rand(11111,99999).'.'.$extension; // renameing image
		$request->file('dokumen_kontrak')->move('upload/dokumen_kontrak', $fileName); // uploading file to given path
		$extension = $request->file('dokumen_transfer_dana')->getClientOriginalExtension(); // getting image extension
		$fileName2 = rand(11111,99999).'.'.$extension; // renameing image
		$request->file('dokumen_transfer_dana')->move('upload/dokumen_transfer_dana', $fileName2); // uploading file to given path
		$data = $request->all();
		$data['dokumen_kontrak'] = $fileName;
		$data['dokumen_transfer_dana'] = $fileName2;
		Upt::create($data);
		// sending back with message
		Session::flash('success', 'Berhasil disimpan'); 
		return redirect('/upt');
  	}

  	public function edit($id){
  		$data = Upt::find($id);
  		$rk = RekanKerja::all();
  		return view('upt.edit', compact('data', 'rk'));
  	}

  	public function update(UptEditRequest $request, $id){
  		$data = Upt::find($id)->update($request->all());
  		Session::flash('success', 'Berhasil dirubah'); 
  		return redirect('/upt');
  	}

  	public function destroy($id){
  		$data = Upt::find($id);
  		File::delete('upload/dokumen_kontrak/'.$data->dokumen_kontrak);
  		File::delete('upload/dokumen_transfer_dana/'.$data->dokumen_transfer_dana);
  		$data->delete();
  		Session::flash('success', 'Data terhapus'); 
		return redirect('/upt');
  	}

}
