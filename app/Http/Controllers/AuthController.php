<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Session;

class AuthController extends Controller
{
	public function postLogin(Request $request){
		if(Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')], $request->input('remember'))){
			Session::set('success', 'Login berhasil');
			return redirect('/dashboard');
		}
		else{
			Session::set('fail', 'Email atau password anda salah');
			return redirect('login');
		}
	}

	public function login(){
		return view('auth.login');
	}

	public function logout(){
		Auth::logout();
		Session::set('success', 'Anda telah logout');
		return redirect('/login');
	}
}
