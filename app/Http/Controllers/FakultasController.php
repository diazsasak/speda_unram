<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Fakultas;
use App\Tagihan;
use DB;

class FakultasController extends Controller
{
	public function index(){
		return Fakultas::all();
	}
	public function getDataFakultas(Request $r){
		$r = $r->all();
		$id = $r['idFakultas']; 
		if($r['tahun'] != ''){
			$tahun = $r['tahun'].$r['semester'];
			$q = " AND tagihan.tahun = $tahun";
		}
		else{
			$q = "";
		}
		$data = DB::select(DB::raw("SELECT tagihan.id, prodi.id, prodi.nama, sum(grade.nominal) as nominal FROM tagihan, mahasiswa, grade, prodi, fakultas WHERE tagihan.id_mahasiswa = mahasiswa.id AND mahasiswa.id_grade = grade.id AND mahasiswa.id_prodi = prodi.id AND prodi.id_fakultas = $id AND tagihan.tanggal_bayar <> '' $q GROUP BY prodi.id"));
		return $data;
	}
	public function getDataAllFakultas(Request $r){
		$r = $r->all();
		if($r['tahun'] != ''){
			$tahun = $r['tahun'].$r['semester'];
			$q = " AND tagihan.tahun = $tahun";
		}
		else{
			$q = "";
		}
		$data = DB::select(DB::raw("SELECT tagihan.id, fakultas.id, fakultas.nama, sum(grade.nominal) as nominal FROM tagihan, mahasiswa, grade, prodi, fakultas WHERE tagihan.id_mahasiswa = mahasiswa.id AND mahasiswa.id_grade = grade.id AND mahasiswa.id_prodi = prodi.id AND prodi.id_fakultas = fakultas.id  AND tagihan.tanggal_bayar <> '' $q GROUP BY fakultas.id"));
		return $data;
	}
	public function getDataSppUkt(Request $r){
		$r = $r->all();
		if($r['tahun'] != ''){
			$tahun = $r['tahun'].$r['semester'];
			$query1 = " AND tagihan.tahun = $tahun";
			$b = $r['tahun'];
			$query2 = " WHERE tanggal_transfer LIKE '%$b%'";
		}
		else{
			$query1 = $query2 = '';
		}
		$spp = DB::select(DB::raw("SELECT sum(grade.nominal) as nominal FROM tagihan, mahasiswa, grade WHERE tagihan.id_mahasiswa = mahasiswa.id AND mahasiswa.id_grade = grade.id AND tagihan.tanggal_bayar <> '' $query1"));
		$ukt = DB::select(DB::raw("SELECT sum(besar_dana) as nominal FROM kontrak_kerjasama_upt $query2"));
		$data = [
		[
		'nama' => 'SPP',
		'nominal' => $spp[0]->nominal
		],
		[
		'nama' => 'UKT',
		'nominal' => $ukt[0]->nominal
		]
		];
		return $data;
	}
}
