<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Excel;
use File;
use App\Fakultas;
use App\Prodi;
use App\Grade;
use App\Mahasiswa;
use App\Tagihan;
use Session;

class SppController extends Controller
{
	public function uploadSpp(){
		return view('spp.upload_spp');
	}
	public function postUploadSpp(Request $r){
		$a = $r->file('file_spp');
		$fileName = $a->getClientOriginalName(); // renameing image
		$r->file('file_spp')->move('upload/spp', $fileName); // uploading file to given path
		$a = Excel::selectSheetsByIndex(0)->load('public/upload/spp/'.$fileName)->get();
		$prodi = $nominalGrade = '';
		$tahun = substr($a[0]->periode_open,0,4);
		$smstr = $tahun.$r->input('semester');
		foreach ($a as $b) {
			//import fakultas
			$f = Fakultas::firstOrCreate(['nama' => $b->fakultas]);
			//import prodi
			$p = Prodi::firstOrCreate(['id_fakultas' => $f->id, 'nama' => $b->prodi]);
			//import grade
			$nominal = substr($b->subbill_01,11);
			
			$g = Grade::firstOrCreate(['nominal' => $nominal, 'tahun' => $tahun]);
			//import mahasiswa
			$a = Mahasiswa::firstOrCreate([
				'nim' => $b->nim,
				'nama' => $b->nama,
				'id_prodi' => $p->id,
				'id_grade' => $g->id
				]);
			Tagihan::firstOrCreate([
				'id_mahasiswa' => $a->id,
				'tahun' => $smstr
				]);
		}
		Session::flash('success', 'Upload data SPP berhasil');
		return redirect('upload_spp');
	}
	public function uploadDataBank(){
		return view('spp.upload_data_bank');
	}
	public function postUploadDataBank(Request $r){
		$a = $r->file('file_data_bank');
		$fileName = $a->getClientOriginalName(); // renameing image
		$r->file('file_data_bank')->move('upload/data_bank', $fileName); // uploading file to given path
		$a = Excel::selectSheetsByIndex(0)->load('public/upload/data_bank/'.$fileName)->get();
		$y = substr($fileName,0,4);
		$m = substr($fileName,4,2);
		$d = substr($fileName,6,2);
		$tgl = $y."-".$m."-".$d;
		$smstr = $y.$r->input('semester');
		foreach ($a as $b) {
			$a = Mahasiswa::where('nim', $b->nim)->first();
			if($a != NULL){
				$a->tagihan()->where('tahun', $smstr)->update(['tanggal_bayar' => $tgl]);
			}
		}
		Session::flash('success', 'Upload data bank berhasil');
		return redirect('upload_data_bank');
	}
	public function index(){
		$data = Tagihan::paginate(8);
		$prodi = Prodi::all();
		return view('spp.status_pembayaran_spp', compact('data', 'prodi'));
	}
	public function postIndex(Request $r){
		$r = $r->all();
		$tahun = $r['tahun'];
		$prodi = $r['prodi'];
		if(isset($r['semester'])){
			$semester = $r['semester'];
		}
		$nimNama = $r['nimNama'];
		if($tahun == null && $prodi == '-'){
			$data = Tagihan::whereHas('mahasiswa', function ($query) use($nimNama) {
				if($nimNama != null){
					$query->where('nim', 'LIKE', "%$nimNama%")
					->orWhere('nama', 'LIKE', "%$nimNama%");
				}
			})->paginate(8);
		}
		else if($tahun != null && $prodi == '-'){
			$tahun = $tahun.$semester;
			$data = Tagihan::whereHas('mahasiswa', function ($query) use($nimNama) {
				if($nimNama != null){
					$query->where('nim', 'LIKE', "%$nimNama%")
					->orWhere('nama', 'LIKE', "%$nimNama%");
				}
			})->where('tahun', $tahun)->paginate(8);
		}
		else if($tahun == null && $prodi != '-'){
			$data = Tagihan::whereHas('mahasiswa', function ($query) use($nimNama, $prodi) {
				if($nimNama != null){
					$query->where('id_prodi', $prodi)->where('nim', 'LIKE', "%$nimNama%")
					->orWhere('nama', 'LIKE', "%$nimNama%");
				}
				else{
					$query->where('id_prodi', $prodi);
				}
			})->paginate(8);
		}
		else if($tahun != null && $prodi != '-'){
			$tahun = $tahun.$semester;
			$data = Tagihan::whereHas('mahasiswa', function ($query) use($nimNama, $prodi) {
				if($nimNama != null){
					$query->where('id_prodi', $prodi)->where('nim', 'LIKE', "%$nimNama%")
					->orWhere('nama', 'LIKE', "%$nimNama%");
				}
				else{
					$query->where('id_prodi', $prodi);
				}
			})->where('tahun', $tahun)->paginate(8);
		}
		$prodi = Prodi::all();
		return view('spp.status_pembayaran_spp', compact('data', 'prodi'));
	}
}
