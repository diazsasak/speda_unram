<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UptRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'no_kontrak' => 'required',
        'besar_dana' => 'required',
        'id_rekan_kerja' => 'required',
        'no_rks' => 'required',
        'pic' => 'required',
        'dokumen_kontrak' => 'required|image',
        'dokumen_transfer_dana' => 'required|image',
        'tanggal_transfer' => 'required|date'
        ];
    }

    public function messages()
    {
        return [
        'required' => 'Semua isian harus diisi',
        'image' => 'Harap upload file bertipe gambar'
        ];
    }
}
