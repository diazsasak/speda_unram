//LOKASI
angular.module('spedaApp.controllers')
.controller('UserCtrl', ['$http', '$scope', '$filter', '$window',
	function($http, $scope, $filter, $window){
		$scope.roles = $window.roles;
		$scope.permissions = $window.permissions;
		$scope.inputData = {
			username: '',
			name: '',
			password: '',
			role: null,
			permissions: []
		};
		if ($window.user) {
			$scope.inputData.username = $window.user.username;
			$scope.inputData.name = $window.user.name;
			$scope.inputData.role = $window.user.roles[0].id;
			$scope.selectedRole = $window.user.roles[0];
			for (var i = 0; i < $window.user.all_permissions.length; i++) {
				$scope.inputData.permissions.push($window.user.all_permissions[i].id);
			}
			console.log($window.user);
			console.log($scope.inputData);
		} else {
			$scope.selectedRole = $scope.roles[0];
		}

		$scope.toggleSelection = function toggleSelection(id) {
			var idx = $scope.inputData.permissions.indexOf(id);

			if (idx > -1) {
				$scope.inputData.permissions.splice(idx, 1);
			}

			else {
				$scope.inputData.permissions.push(id);
			}
		};

		$scope.save = function() {
			$scope.inputData.role = $scope.selectedRole.id;
			$http({
				url: '/user',
				method: 'POST',
				data: $scope.inputData
			}).then(function(data){
				// console.log(data);
				$window.location.href = $window.baseUrl+'/user';
			});
		}

		$scope.update = function() {
			$scope.inputData.role = $scope.selectedRole.id;
			console.log($scope.inputData);
			$http({
				url: '/user/'+$window.user.id,
				method: 'PUT',
				data: $scope.inputData
			}).then(function(data){
				// console.log(data);
				$window.location.href = $window.baseUrl+'/user';
			});
		}
	}]);