//LOKASI
angular.module('spedaApp.controllers')
.controller('LaporanCtrl', ['$http', '$scope', '$filter',
	function($http, $scope, $filter){
		$scope.filterData = {
			tahun: '',
			kategori: '',
			semester: '',
			idFakultas: ''
		};
		$scope.getFakultas = function(){
			$http({
				url: 'fakultas',
				method: 'GET'
			}).then(function(data){
				$scope.fakultas = data.data;
			});
		}
		$scope.getFakultas();
		$scope.cekKategori = function(){
			$scope.idFakultas = 0;
			if($scope.filterData.kategori == 'semuaFakultas'){
				$http({
					url: 'get_data_all_fakultas',
					method: 'POST',
					data: $scope.filterData
				}).then(function(data){
					console.log(data);
					$scope.createChart(data.data);
				});
			}
			else if($scope.filterData.kategori == 'sppDanUkt'){
				$http({
					url: 'get_data_spp_ukt',
					method: 'POST',
					data: $scope.filterData
				}).then(function(data){
					console.log(data);
					$scope.createChart(data.data);
				});
			}
			else if($scope.filterData.kategori == 'perFakultas'){
				$scope.getDataFakultas();
			}
		}
		$scope.createChart = function(data){
			 $('#myChart').remove(); // this is my <canvas> element
			 $('#canvas-container').append('<canvas id="myChart" width="200px" height="200px"></canvas>');
			 var ctx = document.getElementById("myChart");
			 var a = {
			 	labels: [],
			 	datasets: [{
			 		label: '# of Votes',
			 		data: [],
			 		backgroundColor: [],
			 		borderColor: [],
			 		borderWidth: 1
			 	}]
			 };
			 for(var i = 0; i < data.length; i++){
			 	var b = $filter('currency')(parseInt(data[i].nominal), "Rp.", 0);
			 	a.labels.push(data[i].nama+" "+b);
			 	a.datasets[0].data.push(data[i].nominal);
			 	a.datasets[0].backgroundColor.push($scope.dynamicColors());
			 	a.datasets[0].borderColor.push($scope.dynamicColors());
			 };
			 var myChart = new Chart(ctx, {
			 	type: 'pie',
			 	data: a,
			 	options: {
			 		scales: {
			 			yAxes: [{
			 				ticks: {
			 					beginAtZero:true
			 				}
			 			}]
			 		}
			 	}
			 });
			};
			$scope.getDataFakultas = function(){
				$http({
					url: 'get_data_fakultas/',
					method: 'POST',
					data: $scope.filterData
				}).then(function(data){
					$scope.createChart(data.data);
				});
			};
			$scope.dynamicColors = function() {
				var r = Math.floor(Math.random() * 255);
				var g = Math.floor(Math.random() * 255);
				var b = Math.floor(Math.random() * 255);
				return "rgb(" + r + "," + g + "," + b + ")";
			}
		}]);