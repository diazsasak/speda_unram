@extends('layout')
@section('title')
Tambah Grade
@endsection
@section('head')
@endsection
@section('main-title')
Tambah Grade
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<h5>#</h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<div class="#"> 	
				<form method="POST" enctype="multipart/form-data" action="<% url('grade') %>">
					<% csrf_field() %>
					<div class="form-group"><label>Fakultas</label> <input type="text" name="fakultas" class="form-control"></div>
					<div class="form-group"><label>Jurusan / Program Studi</label> <input type="text" name="prodi" class="form-control"></div>
					<div class="form-group"><label>Grade</label><input type="number" name="grade" class="form-control"></div>
					<div class="form-group"><label>Nominal</label> <input type="number" name="nominal" class="form-control"></div>
					<div>
						<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Simpan</strong></button>
						<a class="btn btn-sm btn-primary pull-right m-t-n-xs" href="<% url('/grade') %>"><strong>Batal</strong></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') %>"></script>
<script>
$('#data_3 .input-group.date').datepicker({
				format: 'yyyy-mm-dd',
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });
</script>
@endsection