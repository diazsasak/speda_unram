@extends('layout')
@section('title')
Grade
@endsection
@section('main-title')
Grade
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<button class="btn btn-primary dim" onclick="document.location ='<% url('grade/create') %>'"><i class="fa fa-plus"></i></button>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<!-- <a class="close-link">
					<i class="fa fa-times"></i>
				</a> -->
			</div>
		</div>
		<div class="ibox-content">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>NO</th>
						<th>Fakultas</th>
						<th>Jurusan / Prodi</th>
						<th>Grade</th>
						<th>Nominal</th>
					</tr>
				</thead>
				<tbody>
				@foreach($data as $d)
				<tr>
				<td></td>
				<td>
				<% $d->fakultas %>
				</td>
				<td>
				<% $d->prodi %>
				</td>
				<td>
				<% $d->grade %>
				</td>
				<td>
				<% $d->nominal %>
				</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection