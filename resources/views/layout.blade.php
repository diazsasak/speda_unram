<!DOCTYPE html>
<html ng-app="spedaApp">


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.3/empty_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2015 13:14:14 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SPEDA | @yield('title')</title>

    <link href="<% asset('inspinia/css/bootstrap.min.css') %>" rel="stylesheet">
    <link href="<% asset('inspinia/font-awesome/css/font-awesome.css') %>" rel="stylesheet">

    <link href="<% asset('inspinia/css/animate.css') %>" rel="stylesheet">
    <link href="<% asset('inspinia/css/style.css') %>" rel="stylesheet">
    <link href="<% asset('inspinia/css/plugins/select2/select2.min.css') %>" rel="stylesheet">
    @yield('head')
</head>

<body>

    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            <!-- <img alt="image" class="img-circle" src="img/profile_small.jpg" /> -->
                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Sistem Informasi Keuangan Universitas Mataram</strong>
                            </span>
                        </div>
                        <div class="logo-element">
                            SIKU
                        </div>
                    </li>
                    @permission('manage-user')
                    <li>
                        <a href="<% url('/user') %>"> <span class="nav-label">Manajemen User</span></a>
                    </li>
                    @endpermission
                    @permission('upload-data-spp')
                    <li>
                        <a href="<% url('/upload_spp') %>"> <span class="nav-label">Upload data SPP</span></a>
                    </li>
                    @endpermission
                    @permission('upload-data-bank')
                    <li>
                        <a href="<% url('upload_data_bank') %>"> <span class="nav-label">Upload data Bank</span></a>
                    </li>
                    @endpermission
                    <li>
                        <a href="<% url('status_pembayaran_spp') %>"> <span class="nav-label">Status Pembayaran</span></a>
                    </li>
                    <li>
                        <a href="<% url('laporan') %>"> <span class="nav-label">Laporan</span></a>
                    </li>
                    <li>
                        <a href="<% url('/upt') %>"> <span class="nav-label">Kontrak Kerjasama UPT</span></a>
                    </li>
                    <li>
                        <a href="<% url('/rekankerja') %>"> <span class="nav-label">Rekan Kerja</span></a>
                    </li>
                    <li>
                        <a href="<% url('/grade') %>"> <span class="nav-label">Grade</span></a>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <span class="m-r-sm text-muted welcome-message"><% Auth::user()->name %> - <% Auth::user()->roles()->first()->display_name %></span>
                        </li>
                        @if(Auth::check())
                        <li>
                            <a href="<% url('logout') %>">
                                <i class="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                        @endif
                    </ul>

                </nav>
            </div>

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>@yield('main-title')</h2>
                </div>
            </div>

            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-xs-4 col-xs-offset-4">
                      <%-- pesan error --%>
                      @if(count($errors) > 0)
                      <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <ul>
                          @foreach($errors->all() as $error)
                          <li>{!! $error !!}</li>
                          @endforeach 
                      </ul>
                  </div>
                  @endif
                  <%-- pesan sukses --%>
                  @if(Session::has('success'))
                  <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <% Session::pull('success') %>
                </div>
                @endif
                <%-- pesan gagal --%>
                @if(Session::has('fail'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <% Session::pull('fail') %>
                </div>
                @endif
            </div>
        </div>
        <div class="row">
        <div class="col-md-12">
                @yield('content')
            </div>
        </div>
    </div>
    <div class="footer">
        <div>
            <strong>Copyright</strong> Universitas Mataram &copy; <% date('Y') %>
        </div>
    </div>

</div>
</div>

<!-- Mainly scripts -->
<script src="<% asset('inspinia/js/jquery-2.1.1.js') %>"></script>
<script src="<% asset('inspinia/js/bootstrap.min.js') %>"></script>
<script src="<% asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js') %>"></script>
<script src="<% asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') %>"></script>
<script src="<% asset('inspinia/js/plugins/select2/select2.full.min.js') %>"></script>

<script type="text/javascript" src="<% asset('bower_components/angular/angular.min.js') %>"></script>
<script src="<% asset('app/app.js') %>" type="text/javascript"></script>
<script src="<% asset('app/controllers.js') %>" type="text/javascript"></script>

<script type="text/javascript">
        $(document).ready(function(){
            $('.select2').select2();
        });
    </script>
<!-- Custom and plugin javascript -->
<script src="<% asset('inspinia/js/inspinia.js') %>"></script>
<script src="<% asset('inspinia/js/plugins/pace/pace.min.js') %>"></script>
@yield('script')

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.3/empty_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2015 13:14:14 GMT -->
</html>
