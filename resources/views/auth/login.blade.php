<!DOCTYPE html>
<html>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.3/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2015 13:14:14 GMT -->
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>SIKU | Log In</title>

  <link href="<% asset('inspinia/css/bootstrap.min.css') %>" rel="stylesheet">
  <link href="<% asset('inspinia/font-awesome/css/font-awesome.css') %>" rel="stylesheet">

  <link href="<% asset('inspinia/css/animate.css') %>" rel="stylesheet">
  <link href="<% asset('inspinia/css/style.css') %>" rel="stylesheet">

</head>

<body class="gray-bg">

  <div class="loginColumns animated fadeInDown">
    <div class="row">
      <div class="col-xs-6 col-xs-offset-3">
        <%-- pesan error --%>
        @if(count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <ul>
            @foreach($errors->all() as $error)
            <li>{!! $error !!}</li>
            @endforeach 
          </ul>
        </div>
        @endif
        <%-- pesan sukses --%>
        @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <% Session::pull('success') %>
        </div>
        @endif
        <%-- pesan gagal --%>
        @if(Session::has('fail'))
        <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <% Session::pull('fail') %>
        </div>
        @endif
      </div>
    </div>
    <div class="row">

      <div class="col-md-6 text-center">
        <img src="<% asset('img/unram.png') %>" height="270px" width="270px" />
      </div>
      <div class="col-md-6">
        <div class="ibox-content">
          <h4 class="font-bold">Selamat Datang di SIKU UNRAM</h4>
          <form class="m-t" role="form" method="POST" action="<% url('/login') %>">
            <% csrf_field() %>
            <div class="form-group">
              <input type="text" name="username" class="form-control" placeholder="Username" required="">
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            <a href="#">
              <small></small>
            </a>
          </form>
          <p class="m-t">
            <small>SIKU UNRAM &copy; <% date('Y') %></small>
          </p>
        </div>
      </div>
    </div>
    <hr/>
    <div class="row">
      <div class="col-md-6">
        Universitas Mataram
      </div>
      <div class="col-md-6 text-right">
       <small>© <% date('Y') %></small>
     </div>
   </div>
 </div>
 <!-- Mainly scripts -->
 <script src="<% asset('inspinia/js/jquery-2.1.1.js') %>"></script>
 <script src="<% asset('inspinia/js/bootstrap.min.js') %>"></script>
 <script src="<% asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js') %>"></script>
 <script src="<% asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') %>"></script>

 <!-- Custom and plugin javascript -->
 <script src="<% asset('inspinia/js/inspinia.js') %>"></script>
 <script src="<% asset('inspinia/js/plugins/pace/pace.min.js') %>"></script>
</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.3/login_two_columns.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 01 Sep 2015 13:14:14 GMT -->
</html>
