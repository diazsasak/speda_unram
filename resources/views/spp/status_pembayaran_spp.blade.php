@extends('layout')
@section('title')
Upload SPP
@endsection
@section('main-title')
Upload SPP
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<h5>#</h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<form role="form" action="<% url('post_status_pembayaran_spp') %>" method="GET" class="form-inline">
				<div class="form-group">
					<label for="exampleInputEmail2" class="sr-only">Tahun</label>
					<input type="text" name="tahun" placeholder="Tahun" id="exampleInputEmail2" class="form-control">
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2" class="sr-only">Semester</label>
					<input type="radio" name="semester" id="exampleInputEmail2" value="1" class="form-control"> Ganjil
					<input type="radio" name="semester" id="exampleInputEmail2" value="2" class="form-control"> Genap
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2" class="sr-only">Prodi</label>
					<select class="form-control select2" name="prodi">
						<option value="-" selected>Prodi</option>
						@foreach($prodi as $p)
						<option value="<% $p->id %>"><% $p->nama %></option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2" class="sr-only">NIM / Nama</label>
					<input type="text" name="nimNama" placeholder="NIM / Nama" id="exampleInputEmail2" class="form-control">
				</div>
				<button class="btn btn-success" type="submit">Saring</button>
				<a href="<% url('status_pembayaran_spp') %>" class="btn btn-success" type="submit">Semua</a>
			</form>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Tahun</th>
						<th>Nama</th>
						<th>NIM</th>
						<th>Prodi</th>
						<th>Status</th>
						<th>Tanggal Bayar</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $a)
					<tr>
						<td><% $a->tahun %></td>
						<td><% $a->mahasiswa->nama %></td>
						<td><% $a->mahasiswa->nim %></td>
						<td><% $a->mahasiswa->prodi->nama %></td>
						<td>
							@if($a->tanggal_bayar != null)
							<p class="badge badge-primary">Lunas</p>
							@else
							<p class="badge">Belum Lunas</p>
							@endif
						</td>
						<td><% $a->tanggal_bayar %><td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<% $data->appends([
				'tahun' => Input::get('tahun'),
				'semester' => Input::get('semester'),
				'prodi' => Input::get('prodi'),
				'nimNama' => Input::get('nimNama'),
				])->links() %>
			</div>
		</div>
	</div>
	@endsection