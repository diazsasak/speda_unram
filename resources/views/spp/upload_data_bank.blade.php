@extends('layout')
@section('title')
Upload Data Bank
@endsection
@section('main-title')
Upload Data Bank
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<h5>#</h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<div class="#"> 	
				<form method="POST" enctype="multipart/form-data" action="<% url('upload_data_bank') %>">
					<% csrf_field() %>
					<div class="form-group"><label>Semester</label> 
						<input type="radio" name="semester" value="1" required> Ganjil
						<input type="radio" name="semester" value="2" required> Genap
					<div>
					<div class="form-group"><label>File Data Bank</label> 
						<input type="file" name="file_data_bank" class="form-control" required></div>
					<div>
						<button class="btn btn-sm btn-primary m-t-n-xs" type="submit"><strong>Upload</strong></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection