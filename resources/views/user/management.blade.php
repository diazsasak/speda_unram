@extends('layout')
@section('title')
Manajemen User
@endsection
@section('main-title')
Manajemen User
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<button class="btn btn-primary dim" onclick="document.location ='<% url('user/create') %>'"><i class="fa fa-plus"></i></button>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<!-- <a class="close-link">
					<i class="fa fa-times"></i>
				</a> -->
			</div>
		</div>
		<div class="ibox-content">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Username</th>
						<th>Role & Permissions</th>
						<th>Edit</th>
						<th>Hapus</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $a)
					<tr>
						<td><% $a->username %></td>
						<td>
							<b>Role:</b>
							@foreach ($a->roles as $role)
							<% $role->display_name %> <br/>
							@endforeach
							<b>Permissions:</b> <br/>
							@foreach ($a->allPermissions() as $p) 
							<% $p->display_name %> <br/>
							@endforeach
						</td>
						<td>
							<button class="btn btn-success btn-circle dim" onclick="document.location ='<% url('user/'.$a->id.'/edit') %>'"><i class="fa fa-edit"></i></button>
						</td>
						<td>
							<form action="<% url('user/'.$a->id) %>" method="POST" >
								<% method_field('DELETE') %>
								<% csrf_field() %>
								<button class="btn btn-danger btn-circle dim" type="submit"><i class="fa fa-trash"></i></button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection