@extends('layout')
@section('title')
Tambah User
@endsection
@section('head')
<link href="<% asset('inspinia/css/plugins/datapicker/datepicker3.css') %>" rel="stylesheet">
@endsection
@section('main-title')
Tambah User
@endsection
@section('content')
<div class="col-lg-12" ng-controller="UserCtrl">
	<div class="ibox">
		<div class="ibox-title">
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<div class="#"> 	
				<form method="POST" enctype="multipart/form-data" action="<% url('user') %>">
					<% csrf_field() %>
					<div class="form-group">
						<label>Nama</label> 
						<input type="text" name="name" class="form-control" ng-model="inputData.name">
					</div>
					<div class="form-group">
						<label>Username</label> 
						<input type="text" name="username" class="form-control" ng-model="inputData.username">
					</div>
					<div class="form-group">
						<label>Password</label> 
						<input type="text" name="password" class="form-control" ng-model="inputData.password">
					</div>
					<div class="form-group">
						<label>Role</label> 
						<select class="form-control" name="role" ng-model="selectedRole">
							<option ng-repeat="r in roles" ng-value="r">{{ r.display_name }}</option>
						</select>
					</div>
					<div class="form-group" ng-if="selectedRole.name == 'operator'">
						<label>Permissions</label> 
						<div class="checkbox" ng-repeat="p in permissions">
							<label>
								<input type="checkbox" name="selectedPermissions[]"
								value="{{p.id}}"
								ng-checked="inputData.permissions.indexOf(p.id) > -1"
								ng-click="toggleSelection(p.id)">
								{{ p.display_name }}
							</label>
						</div>
					</div>
					<div>
						<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="button" ng-click="save()"><strong>Simpan</strong></button>
						<a class="btn btn-sm btn-primary pull-right m-t-n-xs" href="<% url('/user') %>"><strong>Batal</strong></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	var baseUrl = '{!! url('/') !!}';
	var roles = {!! $roles !!};
	var permissions = {!! $permissions !!}
</script>
<script src="<% asset('app/controllers/user.js') %>" type="text/javascript"></script>
@endsection