@extends('layout')
@section('title')
Laporan
@endsection
@section('main-title')
Laporan
@endsection
@section('content')
<div class="col-lg-12" ng-controller="LaporanCtrl">
	{{ tes }}
	<div class="ibox">
		<div class="ibox-title">
			<h5>#</h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<form role="form" action="#" method="POST" class="form-inline">
				<div class="form-group">
					<label for="exampleInputEmail2" class="sr-only">Tahun</label>
					<input type="text" name="tahun" placeholder="Tahun" ng-model="filterData.tahun" id="exampleInputEmail2" class="form-control">
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2" class="sr-only">Semester</label>
					<input type="radio" name="semester" id="exampleInputEmail2" ng-model="filterData.semester" value="1" class="form-control"> Ganjil
					<input type="radio" name="semester" id="exampleInputEmail2" ng-model="filterData.semester" value="2" class="form-control"> Genap
				</div>
				<div class="form-group">
					<label for="exampleInputEmail2" class="sr-only">Kategori</label>
					<select class="form-control" name="kategori" ng-model="filterData.kategori" ng-change="cekKategori()">
						<option value="semuaFakultas">Semua Fakultas</option>
						<option value="perFakultas">Per Fakultas</option>
						<option value="sppDanUkt">SPP dan UKT</option>
					</select>
				</div>
				<div class="form-group" ng-show="filterData.kategori == 'perFakultas'">
					<label for="exampleInputEmail2" class="sr-only">Fakultas</label>
					<select class="form-control" name="fakultas" ng-model="filterData.idFakultas" ng-change="getDataFakultas()">
						<option ng-repeat="a in fakultas" value="{{a.id}}">{{a.nama}}</option>
					</select>
				</div>
				<div class="btn btn-primary" ng-click="cekKategori()">Tampil</div>
			</form>
			<div class="col-lg-6 col-lg-offset-3" id="canvas-container">
				<canvas id="myChart" width="200px" height="200px"></canvas>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset('bower_components/chart.js/dist/Chart.min.js') %>"></script>
<script src="<% asset('app/controllers/laporan.js') %>" type="text/javascript"></script>
@endsection