@extends('layout')
@section('title')
Tambah Kontrak UPT
@endsection
@section('head')
<link href="<% asset('inspinia/css/plugins/datapicker/datepicker3.css') %>" rel="stylesheet">
@endsection
@section('main-title')
Tambah Kontrak UPT
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<div class="#"> 	
				<form method="POST" enctype="multipart/form-data" action="<% url('upt') %>">
					<% csrf_field() %>
					<div class="form-group"><label>No Kontrak</label> <input type="text" name="no_kontrak" class="form-control"></div>
					<div class="form-group"><label>Besar Dana</label> <input type="number" name="besar_dana" class="form-control"></div>
					<div class="form-group"><label>Rekan Kerjasama</label> 
						<select class="form-control" name="id_rekan_kerja">
							@foreach($rekankerja as $r)
							<option value="<% $r->id %>"><% $r->rekan_kerja %></option>
							@endforeach
						</select>
					</div>
					<div class="form-group"><label>No RKS</label> <input type="text" name="no_rks" class="form-control"></div>
					<div class="form-group"><label>PIC</label> <input type="text" name="pic" class="form-control"></div>
					<div class="form-group"><label>Dokumen Kontrak</label> <input type="file" name="dokumen_kontrak" class="form-control"></div>
					<div class="form-group"><label>Dokumen Transfer Dana</label> <input type="file" name="dokumen_transfer_dana" class="form-control"></div>
					<div class="form-group" id="data_3">
						<label class="font-noraml">Tanggal Transfer</label>
						<!-- <input type="date" name="tanggal_transfer"> -->
						<div class="input-group date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="tanggal_transfer" value="10/11/2016">
						</div>
					</div>
					<div>
						<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Simpan</strong></button>
						<a class="btn btn-sm btn-primary pull-right m-t-n-xs" href="<% url('/upt') %>"><strong>Batal</strong></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') %>"></script>
<script>
	$('#data_3 .input-group.date').datepicker({
		format: 'yyyy-mm-dd',
		startView: 2,
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		autoclose: true
	});
</script>
@endsection