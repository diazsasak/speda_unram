@extends('layout')
@section('title')
Edit Kontrak UPT
@endsection
@section('head')
<link href="<% asset('inspinia/css/plugins/datapicker/datepicker3.css') %>" rel="stylesheet">
@endsection
@section('main-title')
Edit Kontrak UPT
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<div class="#"> 	
				<form method="POST" enctype="multipart/form-data" action="<% url('upt/'.$data->id) %>">
					<% csrf_field() %>
					<% method_field('PUT') %>
					<div class="form-group"><label>No Kontrak</label> <input type="text" name="no_kontrak" class="form-control" value="<% $data->no_kontrak %>"></div>
					<div class="form-group"><label>Besar Dana</label> <input type="number" name="besar_dana" class="form-control" value="<% $data->besar_dana %>"></div>
					<div class="form-group"><label>Rekan Kerjasama</label> 
						<select class="form-control" value="<% $data->id_rekan_kerja %>" name="id_rekan_kerja">
							@foreach($rk as $r)
							@if($r->id == $data->id_rekan_kerja)
							<option value="<% $r->id %>" selected><% $r->rekan_kerja %></option>
							@else
							<option value="<% $r->id %>"><% $r->rekan_kerja %></option>
							@endif
							@endforeach
						</select>
					</div>
					<div class="form-group"><label>No RKS</label> <input type="text" name="no_rks" class="form-control" value="<% $data->no_rks %>"></div>
					<div class="form-group"><label>PIC</label> <input type="text" name="pic" class="form-control" value="<% $data->pic %>"></div>
					<div class="form-group">
						<label>Dokumen Kontrak</label> <br/>
						<a href="<% url('upload/dokumen_kontrak/'.$data->dokumen_kontrak) %>" target="_blank"><img src="<% asset('upload/dokumen_kontrak/'.$data->dokumen_kontrak) %>" height="100px" width="100px"></a> <br/>
						<input type="file" name="dokumen_kontrak" class="form-control" value="<% $data->dokumen_kontrak %>">
					</div>
					<div class="form-group">
						<label>Dokumen Transfer Dana</label> <br/>
						<a href="<% url('upload/dokumen_transfer_dana/'.$data->dokumen_transfer_dana) %>" target="_blank"><img src="<% asset('upload/dokumen_transfer_dana/'.$data->dokumen_transfer_dana) %>" height="100px" width="100px"></a> <br/>
						<input type="file" name="dokumen_kontrak" class="form-control" value="<% $data->dokumen_kontrak %>">
					</div>
					<div class="form-group" id="data_3">
						<label class="font-noraml">Tanggal Transfer</label>
						<div class="input-group date">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="<% $data->tanggal_transfer %>" name="tanggal_transfer">
						</div>
					</div>
					<div>
						<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Simpan</strong></button>
						<a class="btn btn-sm btn-primary pull-right m-t-n-xs" href="<% url('/upt') %>"><strong>Batal</strong></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') %>"></script>
<script>
	$('#data_3 .input-group.date').datepicker({
		format: 'yyyy-mm-dd',
		startView: 2,
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		autoclose: true
	});
</script>
@endsection