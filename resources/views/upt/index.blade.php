@extends('layout')
@section('title')
Daftar Kontrak UPT
@endsection
@section('main-title')
Daftar Kontrak UPT
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			@if (Auth::user()->can(['add-kku']))
			<button class="btn btn-primary dim" onclick="document.location ='<% url('upt/create') %>'"><i class="fa fa-plus"></i>
			</button>
			@endif
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>No Kontrak</th>
						<th>Besar Dana</th>
						<th>Rekan Kerjasama</th>
						<th>No RKS</th>
						<th>PIC</th>
						<th>Dokumen Kontrak</th>
						<th>Dokumen Transfer Dana</th>
						<th>Tanggal Transfer</th>
						<th>Edit</th>
						<th>Hapus</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $a)
					<tr>
						<td><% $a->no_kontrak %></td>
						<td><% number_format($a->besar_dana) %></td>
						<td><% $a->rk->rekan_kerja %></td>
						<td><% $a->no_rks %></td>
						<td><% $a->pic %></td>
						<td><a href="<% url('upload/dokumen_kontrak/'.$a->dokumen_kontrak) %>" target="_blank">Lihat</a></td>
						<td><a href="<% url('upload/dokumen_transfer_dana/'.$a->dokumen_transfer_dana) %>" target="_blank">Lihat</a></td>
						<td><% $a->tanggal_transfer %></td>
						<td>
							@if (Auth::user()->can(['edit-kku']))
							<button class="btn btn-success btn-circle dim" onclick="document.location ='<% url('upt/'.$a->id.'/edit') %>'"><i class="fa fa-edit"></i>
							</button>
							@else
							&nbsp;
							@endif
						</td>
						<td>
							@if (Auth::user()->can(['delete-kku']))
							<form action="<% url('upt/'.$a->id) %>" method="POST" >
								<% method_field('DELETE') %>
								<% csrf_field() %>
								<button class="btn btn-danger btn-circle dim" type="submit"><i class="fa fa-trash"></i></button>
							</form>
							@else
							&nbsp;
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection