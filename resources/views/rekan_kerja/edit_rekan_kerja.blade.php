@extends('layout')
@section('title')
Tambah Rekan Kerja
@endsection
@section('head')
<link href="<% asset('inspinia/css/plugins/datapicker/datepicker3.css') %>" rel="stylesheet">
@endsection
@section('main-title')
Tambah Rekan Kerja
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			<h5>#</h5>
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="ibox-content">
			<div class="#"> 	
				<form method="POST" action="<% url('rekankerja/'.$data->id) %>">
				<input type="hidden" name="_method" value="PUT">
					<% csrf_field() %>
					<div class="form-group"><label>Rekan Kerja</label> <input type="text" name="rekan_kerja" class="form-control" value="<% $data->rekan_kerja %>"></div>
					<div>
						<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Simpan</strong></button>
						<a class="btn btn-sm btn-primary pull-right m-t-n-xs" href="<% url('/rekankerja') %>"><strong>Batal</strong></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script src="<% asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') %>"></script>
<script>
$('#data_3 .input-group.date').datepicker({
				format: 'yyyy-mm-dd',
                startView: 2,
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });
</script>
@endsection