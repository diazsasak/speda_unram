@extends('layout')
@section('title')
Daftar Rekan Kerja
@endsection
@section('main-title')
Daftar Rekan Kerja
@endsection
@section('content')
<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-title">
			@if (Auth::user()->can(['add-rekan-kerja']))
			<button class="btn btn-primary dim" onclick="document.location ='<% url('rekankerja/create') %>'"><i class="fa fa-plus"></i></button>
			@endif
			<div class="ibox-tools">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="fullscreen-link">
					<i class="fa fa-expand"></i>
				</a>
				<!-- <a class="close-link">
					<i class="fa fa-times"></i>
				</a> -->
			</div>
		</div>
		<div class="ibox-content">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Rekan Kerja</th>
						<th>Edit</th>
						<th>Hapus</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $a)
					<tr>
						<td><% $a->id %></td>
						<td><% $a->rekan_kerja %></td>
						<td>
							@if (Auth::user()->can(['edit-rekan-kerja']))
							<button class="btn btn-success btn-circle dim" onclick="document.location ='<% url('rekankerja/'.$a->id.'/edit') %>'"><i class="fa fa-edit"></i></button>
							@else
							&nbsp;
							@endif
						</td>
						<td>
							@if (Auth::user()->can(['delete-rekan-kerja']))
							<form action="<% url('rekankerja/'.$a->id) %>" method="POST" >
								<% method_field('DELETE') %>
								<% csrf_field() %>
								<button class="btn btn-danger btn-circle dim" type="submit"><i class="fa fa-trash"></i></button>
							</form>
							@else
							&nbsp;
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection